## ucam-app-secrets
Scripts for rotating secrets for app registrations in BLUE AD, eg the wsgetmail
app which feeds email from ExOL to RT

## Description
This provides code to use the Azure CLI to rotate app secrets that apps such as
wsgetmail use to fetch mail, and update their config files with the new
secrets.

## Installation
This requires the Azure CLI to be installed. In Chemistry this would normally
happen via Ansible applying the azure\_cli role.

To configure copy ucam\_app\_secrets/default_config.yml to
/etc/ucam-app-secrets/config.yml, or for testing
ucam\_app\_secrets/config.yml, and edit as needed. Note that your deployed
config.yml will be merged with the application's default_config.yml, so remove
lines from your deployed config.yml where you want to use the application
defaults.

## Usage

The scripts only operate on secrets whose name starts with the string set in
the configuration file. The app may have other secrets, which will be ignored.

rotate-rt-secrets will rotate secrets and print out the latest secret. It will
keep the new secret plus up to as many old ones as the config file says to.

template-wsgetmail-files looks in /etc/wsgetmail/templates for files ending
json.j2 . It rotates the credentials and then templates the new credential into
those files, writing them out as .json files in /etc/wsgetmail . In order that
Ansible can easily write the templates using the template modules, the template
marker used by template-wsgetmail-files for its own templating is '[[' and ']]'
not the standard '{{' and '}}'.
