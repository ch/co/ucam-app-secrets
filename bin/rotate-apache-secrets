#!/usr/bin/python3
import os
import shutil
import sys
from ucam_app_secrets import rotate_creds, AZNotFound, AZError, az_logout

secrets_dir = "/etc/ucam-app-secrets"
current_secret_file = os.path.join(secrets_dir, "apache-auth-openidc-client-secret-current.conf")
next_secret_file = os.path.join(secrets_dir, "apache-auth-openidc-client-secret-next.conf")

# Get a new secret
try:
    new_secret = rotate_creds()
except AZNotFound:
    print("Could not find 'az' command, is Azure CLI installed?")
    sys.exit(1)
except AZError as e:
    print("Error from AZ CLI")
    print(e.__cause__.stderr.decode("utf-8"))
    az_logout()
    sys.exit(e.__cause__.returncode)

def write_secret_file(filename):
    with open(os.open(filename, os.O_CREAT | os.O_TRUNC | os.O_WRONLY, 0o440), "w") as f:
        f.write(f"OIDCClientSecret {new_secret}\n")
    shutil.chown(filename, user="xymon", group="www-data")

# Do we have a next secret file already? If we do, copy it to current
if os.path.exists(next_secret_file):
    shutil.copy(next_secret_file, current_secret_file)
# Do we have a current secret file? If not, write new secret to current
if not os.path.exists(current_secret_file):
    write_secret_file(current_secret_file)
# Write new secret to next
write_secret_file(next_secret_file)
