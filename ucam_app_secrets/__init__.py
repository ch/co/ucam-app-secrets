import os
import yaml

__version__ = "1.4.0"

# Load default config
basedir = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(basedir, "default_config.yml"), "r") as f:
    config = yaml.load(f, yaml.SafeLoader)

# Look for other config files
potential_files = [
    "/etc/ucam-app-secrets/config.yml",
    os.path.join(basedir, "config.yml"),
]
for c in potential_files:
    if os.path.isfile(c) and os.access(c, os.R_OK):
        with open(c, "r") as f:
            config.update(yaml.load(f, yaml.SafeLoader))

from .az_secrets import *  # noqa E402 F401 F403
from .xymon import *  # noqa E402 F401 F403
