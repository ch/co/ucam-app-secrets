#!/usr/bin/python3
import subprocess
import json
import datetime
import dateutil.parser  # datetime.fromisoformat() can't handle AZ's dates

from . import config

default_timeout = 300


class AZError(Exception):
    """Raise when we get a fatal error from AZ"""


class AZNotFound(Exception):
    """Raise when the Azure CLI isn't found"""


class AZTimeout(Exception):
    """Raise when the az process takes too long"""


def az_login():
    try:
        subprocess.run(
            [
                "az",
                "login",
                "--allow-no-subscriptions",
                "-u",
                config["user"],
                "-p",
                config["password"],
            ],
            check=True,
            capture_output=True,
            timeout=config.get("timeout", default_timeout),
        )
    except subprocess.CalledProcessError as e:
        raise AZError("Failed to log in to Azure AD") from e
        print(e.stderr.decode("utf-8"))
    except subprocess.TimeoutExpired as e:
        raise AZTimeout("Timed out logging into Azure") from e
    except FileNotFoundError:
        raise AZNotFound


def get_current_creds(exclude=""):
    # list secrets and find the ones under the control of this script
    try:
        get_creds = subprocess.run(
            ["az", "ad", "app", "credential", "list", "--id", config["app_id"]],
            check=True,
            capture_output=True,
            timeout=config.get("timeout", default_timeout),
        )
    except subprocess.CalledProcessError as e:
        raise AZError("Failed to list credentials") from e
    except subprocess.TimeoutExpired as e:
        raise AZTimeout("Timed out listing credentials") from e
    cred_list = [
        c
        for c in json.loads(get_creds.stdout)
        if (
            (c.get("displayName", "").startswith(config["secret_name_prefix"]))
            and (not exclude.startswith(c.get("hint")))
        )
    ]
    return cred_list


def add_new_cred():
    # choose date
    new_credential_end_date = datetime.date.today() + datetime.timedelta(
        config["secret_lifetime_days"]
    )
    # choose name
    new_credential_name = (
        config["secret_name_prefix"] + new_credential_end_date.isoformat()
    )

    try:
        add_secret = subprocess.run(
            [
                "az",
                "ad",
                "app",
                "credential",
                "reset",
                "--id",
                config["app_id"],
                "--append",
                "--display-name",
                new_credential_name,
                "--end-date",
                new_credential_end_date.isoformat(),
            ],
            check=True,
            capture_output=True,
            timeout=config.get("timeout", default_timeout),
        )
    except subprocess.CalledProcessError as e:
        raise AZError("Failed to add secret") from e
    except subprocess.TimeoutExpired as e:
        raise AZTimeout("Timed out adding secret") from e
    return json.loads(add_secret.stdout).get("password")


def remove_excess_creds(cred_list):
    removed_cred_ids = []
    # Remove the oldest secrets
    if len(cred_list) > config["secrets_to_keep"]:
        # find the oldest cred
        cred_list.sort(key=lambda x: dateutil.parser.isoparse(x["endDateTime"]))
        for cred in cred_list[: -config["secrets_to_keep"]]:
            try:
                subprocess.run(
                    [
                        "az",
                        "ad",
                        "app",
                        "credential",
                        "delete",
                        "--id",
                        config["app_id"],
                        "--key-id",
                        cred.get("keyId"),
                    ],
                    check=True,
                    capture_output=True,
                    timeout=config.get("timeout", default_timeout),
                )
            except subprocess.CalledProcessError as e:
                raise AZError("Failed to remove secret with id {}").format(
                    cred.get("keyId")
                ) from e
            except subprocess.TimeoutExpired as e:
                raise AZTimeout("Timed out removing secret with id {}").format(
                    cred.get("keyId")
                ) from e
            removed_cred_ids.append(cred.get("keyId"))
    return removed_cred_ids


def az_logout():
    subprocess.run(["az", "logout"], timeout=config.get("timeout", default_timeout))


def rotate_creds():
    az_login()
    new_cred_secret = add_new_cred()
    cred_list = get_current_creds(exclude=new_cred_secret)
    remove_excess_creds(cred_list)
    az_logout()
    return new_cred_secret
