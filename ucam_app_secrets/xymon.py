import dateutil.parser
import datetime
from . import az_secrets


def _get_all_creds():
    """
    Get all the creds with our prefix
    """
    try:
        az_secrets.az_login()
        cred_list = az_secrets.get_current_creds()
    except az_secrets.AZNotFound:
        return [], "Azure CLI not found"
    except az_secrets.AZTimeout:
        return [], "Azure CLI timed out"
    except az_secrets.AZError as e:
        az_secrets.az_logout()
        return (
            [],
            "Failed to load secrets:\n {}".format(e.__cause__.stderr.decode("utf-8")),
        )
    az_secrets.az_logout()
    return cred_list, ""


def _get_last_expiring_cred():
    """
    Get the latest cred, returning tuple of cred and error message
    """
    cred_list = _get_all_creds()[0]
    try:
        cred_list.sort(
            key=lambda x: (
                dateutil.parser.isoparse(x["endDateTime"]),
                dateutil.parser.isoparse(x["startDateTime"]),
            )
        )
        last_expiring_cred = cred_list.pop()
    except IndexError:
        return None, "No credentials found with prefix {prefix}"
    return last_expiring_cred, ""


def _get_cred_with_hint(hint):
    """
    get the latest cred matching hint or None
    """
    cred_list = _get_all_creds()[0]
    valid_hints = {
        cred.get("hint"): cred
        for cred in cred_list
        if dateutil.parser.isoparse(cred["endDateTime"])
        >= datetime.datetime.now(datetime.timezone.utc)
    }
    return valid_hints.get(hint)


def xymon_check_cred_age(secret):
    """
    Return a tuple of a Xymon colour string and a message
    """
    colour = "red"
    message = "Could not find cred with prefix {prefix} and hint {hint}"
    days_to_expiry = None
    hint = secret[0:3]
    cred = _get_cred_with_hint(hint)
    if cred:
        days_to_expiry = (
            dateutil.parser.isoparse(cred["endDateTime"])
            - datetime.datetime.now(datetime.timezone.utc)
        ).days
        if days_to_expiry < az_secrets.config["days_before_expiry_red"]:
            colour = "red"
        elif days_to_expiry < az_secrets.config["days_before_expiry_yellow"]:
            colour = "yellow"
        else:
            colour = "green"
        message = "{days} days to expiry of credential with matching hint whose name starts {prefix}"
    return (
        colour,
        message.format(
            days=days_to_expiry,
            prefix=az_secrets.config["secret_name_prefix"],
            hint=hint,
        ),
    )


def xymon_check_newest_cred_age():
    """
    Return a tuple of a Xymon colour string and a message
    """
    cred, cred_message = _get_last_expiring_cred()
    if cred:
        colour, message = xymon_check_cred_age(cred.get("hint"))
        return colour, message
    return "red", cred_message


def xymon_compare_newest_cred(secret):
    """
    Compares its argument with the hint for the newest credential

    Returns tuple of Xymon colour string and message
    """
    colour = "red"
    cred, message = _get_last_expiring_cred()
    if cred:
        if cred["hint"] == secret[0:3]:
            colour = "green"
            message += "Newest secret hint matches secret in use"
        else:
            message += "Newest secret hint {hint} does not match secret in file"
    return (
        colour,
        message.format(
            prefix=az_secrets.config["secret_name_prefix"], hint=cred.get("hint")
        ),
    )


def xymon_check_cred_exists(secret):
    """
    Compares its argument with hints for all the unexpired creds

    Returns tuple of Xymon colour string and message
    """
    message = ""
    colour = "red"
    cred = _get_cred_with_hint(secret[0:3])
    if cred:
        colour = "green"
        message += (
            "There is a valid secret for {prefix} whose hint matches secret in file"
        )
    else:
        message += (
            "Cannot find a valid secret matching hint {hint} in file for {prefix}"
        )
    return (
        colour,
        message.format(
            prefix=az_secrets.config["secret_name_prefix"], hint=secret[0:3]
        ),
    )
